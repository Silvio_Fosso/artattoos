# ARTattoos

This app is written in swift on xcode

Starting from iOS 11.3, ARKit has the capability to recognize 2D images.

In this application you can try your prefered tattoos on your skin !

How? is very simple : 

1)Choose your favorite tattoo from the list

2)Draw an X on the body part where you want to try the tattoo

3)Shoot the X with the camera and that’s it!


![](Image/3.MP4)
![](Image/4.MP4)
![](Image/2.JPG)
![](Image/1.JPG)
