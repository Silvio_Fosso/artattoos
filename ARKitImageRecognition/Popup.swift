

import UIKit

class Popup: UIViewController {
    @IBOutlet weak var rimuov: UIView!
    let kRotationAnimationKey = "a"
    @IBOutlet weak var img: UIImageView!
    var immagine : UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
           
        }
        let purple = UIColor.black
        let purpleTrans = UIColor.withAlphaComponent(purple)(0.8)
        self.view.backgroundColor = purpleTrans
        
        ShowAnimate()
        img.backgroundColor = .clear
        rimuov.backgroundColor = .clear
        rotateView(view: img)
     
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        removeAnimate()
    }
func ShowAnimate()
{
    self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
    self.view.alpha = 0.0
    img.backgroundColor = .clear
    UIView.animate(withDuration: 0.25, animations: {
        self.view.alpha = 1.0
       self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        
    })
}
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        }) { (Prova : Bool) in
           if(Prova)
           {
            self.view.removeFromSuperview()
            }
        }
        
        
        
    }
  
    @IBAction func Try(_ sender: Any) {
      performSegue(withIdentifier: "Ar", sender: self)
    }
    
    func rotateView(view: UIView, duration: Double = 1) {
//        UIView.animate(withDuration: 2.0) {
//
//        }
        UIView.animate(withDuration: 3.0, animations: {
            view.layer.transform = CATransform3DMakeRotation(.pi,0.0,1.0,0.0);
        }) { (Bool) in
            UIView.animate(withDuration: 3.0) {
                view.layer.transform = CATransform3DMakeRotation(.pi/4,0.0,1.0,0.0);
            }
        }
     
  
//    view.layer.transform = t3
       
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "Ar")
        {
            let dis = segue.destination as? ViewController
            dis?.Image =  immagine!
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        immagine = img.image
        img.image = img.image?.maskWithColor(color: .white)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIImage {

    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!

        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)

        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!

        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)

        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }

}
