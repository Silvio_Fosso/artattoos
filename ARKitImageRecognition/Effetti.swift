//
//  Effetti.swift
//  ARKitImageRecognition
//
//  Created by Silvio Fosso on 04/12/2019.

//

import UIKit
import CoreImage
import AudioToolbox
class Effetti: UIViewController {
    @IBOutlet weak var nascondi: UIView!
    var tag = 0
    var alert : UIAlertController!
    @IBOutlet weak var btn4: UIImageView!
    @IBOutlet weak var btn3: UIImageView!
    @IBOutlet weak var btn1: UIImageView!
    @IBOutlet weak var btn: UIImageView!
    var applicato = false
    var currentFilter : CIFilter!
    var currentFilter1 : CIFilter!
    var contex = CIContext()
    var begin : CIImage!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var img: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        nascondi.backgroundColor = .clear
        img.layer.cornerRadius = 8.0
        img.clipsToBounds = true
        img.contentMode = .scaleAspectFill
        let purple = UIColor.black
        let purpleTrans = UIColor.withAlphaComponent(purple)(0.8)
        self.view.backgroundColor = purpleTrans
       ShowAnimate()
          let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(tapGestureRecognizer:)))
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(tapGestureRecognizer:)))
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(tapGestureRecognizer:)))
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(tapGestureRecognizer:)))
        btn.isUserInteractionEnabled = true
        btn.addGestureRecognizer(tapGestureRecognizer)
        btn1.isUserInteractionEnabled = true
        btn1.addGestureRecognizer(tapGestureRecognizer1)
        btn3.isUserInteractionEnabled = true
        btn3.addGestureRecognizer(tapGestureRecognizer2)
        btn4.isUserInteractionEnabled = true
        btn4.addGestureRecognizer(tapGestureRecognizer3)
        
        // Do any additional setup after loading the view.
    }
    @IBAction func save(_ sender: Any) {
        alert = UIAlertController(title: "Saving in Progress", message: "Please Wait...", preferredStyle: .actionSheet)

       

        self.present(alert, animated: true)
        UIImageWriteToSavedPhotosAlbum(img.image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        
        
        
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {

          if let error = error {
              print("Error Saving ARKit Scene \(error)")
          } else {
            
           print("ok")
             AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            self.alert.dismiss(animated: true, completion: nil)
          }
      }
    
    
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
    img.image = nil
    if(tapGestureRecognizer.view?.tag == 1)
    {
        
      img.image = btn.image?.addFilter(filter: .Chrome)
    } else if(tapGestureRecognizer.view?.tag == 2)
    {
      img.image = btn1.image?.addFilter(filter: .Mono)
    } else if(tapGestureRecognizer.view?.tag == 3)
       {
         img.image = btn3.image?.addFilter(filter: .Process)
       } else if(tapGestureRecognizer.view?.tag == 4)
          {
            img.image = btn4.image?.addFilter(filter: .Transfer)
          }
          
        
       }
    enum FilterType : String {
    case Chrome = "CIPhotoEffectChrome"
    case Fade = "CIPhotoEffectFade"
    case Instant = "CIPhotoEffectInstant"
    case Mono = "CIPhotoEffectMono"
    case Noir = "CIPhotoEffectNoir"
    case Process = "CIPhotoEffectProcess"
    case Tonal = "CIPhotoEffectTonal"
    case Transfer =  "CIPhotoEffectTransfer"
    }
   
    
    @IBAction func Share(_ sender: Any) {
        let imageToShare = [ img.image! ]
              let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
              activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

              // exclude some activity types from the list (optional)
           

              // present the view controller
              self.present(activityViewController, animated: true, completion: nil)
    }
    

    override func viewDidAppear(_ animated: Bool) {
      
       let imga = img.image!.addFilter(filter: .Chrome)
        btn.image = imga
        btn.contentMode = .scaleAspectFill
        btn.layer.cornerRadius = 8.0
        btn.clipsToBounds = true
        
        
        let imga1 = img.image!.addFilter(filter: .Mono)
               btn1.image = imga1
               btn1.contentMode = .scaleAspectFill
               btn1.layer.cornerRadius = 8.0
               btn1.clipsToBounds = true
        
        let imga2 = img.image!.addFilter(filter: .Process)
        btn3.image = imga2
        btn3.contentMode = .scaleAspectFill
        btn3.layer.cornerRadius = 8.0
        btn3.clipsToBounds = true
        
        
        let imga3 = img.image!.addFilter(filter: .Transfer)
               btn4.image = imga3
               btn4.contentMode = .scaleAspectFill
               btn4.layer.cornerRadius = 8.0
               btn4.clipsToBounds = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       if let touch = touches.first {
        let position = touch.location(in: view)
        if(position.y > 585)
        {
          removeAnimate()
        }
        }
    }
 
    @IBAction func Value(_ sender: UISlider) {
       
    }
    
  
    func ShowAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        img.backgroundColor = .clear
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
           self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        })
    }
        func removeAnimate() {
            UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
            }) { (Prova : Bool) in
               if(Prova)
               {
                self.view.removeFromSuperview()
                }
            }
            
            
            
        }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIImage {
    enum FilterType : String {
    case Chrome = "CIPhotoEffectChrome"
    case Fade = "CIPhotoEffectFade"
    case Instant = "CIPhotoEffectInstant"
    case Mono = "CIPhotoEffectMono"
    case Noir = "CIPhotoEffectNoir"
    case Process = "CIPhotoEffectProcess"
    case Tonal = "CIPhotoEffectTonal"
    case Transfer =  "CIPhotoEffectTransfer"
    }
func addFilter(filter : FilterType) -> UIImage {
let filter = CIFilter(name: filter.rawValue)
// convert UIImage to CIImage and set as input
let ciInput = CIImage(image: self)
filter?.setValue(ciInput, forKey: "inputImage")
// get output CIImage, render as CGImage first to retain proper UIImage scale
let ciOutput = filter?.outputImage
let ciContext = CIContext()
let cgImage = ciContext.createCGImage(ciOutput!, from: (ciOutput?.extent)!)
//Return the image
return UIImage(cgImage: cgImage!)
}
}
