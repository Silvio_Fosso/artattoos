


import UIKit
import ARKit
import MBCircularProgressBar
class ViewController: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    var isTorchOn = false
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var photo: UIBarButtonItem!
    @IBOutlet weak var ProgressBar: MBCircularProgressBarView!
    var Image = UIImage()
    var SalvaJpg = false
    var flashView = UIView()
    
    
    lazy var X: SCNNode = {
        guard let scene = SCNScene(named: "X.scn"),
            let node = scene.rootNode.childNode(withName: "X", recursively: false) else { return SCNNode() }
        let scaleFactor  = 0.25
        node.scale = SCNVector3(scaleFactor, scaleFactor, scaleFactor)
        node.eulerAngles.x += -.pi / 2
        
        return node
    }()
    
    override func viewDidLoad() {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        super.viewDidLoad()
        sceneView.delegate = self
        configureLighting()
        flashView = UIView(frame: sceneView.frame)
        flashView.alpha = 0
        flashView.backgroundColor = UIColor.black
        sceneView.addSubview(flashView)
        ProgressBar.value = 0
        
    }
    
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    func configureLighting() {
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        resetTrackingConfiguration()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            self?.toggleTorch(on: true)
        }
    }
    func toggleTorch(on: Bool) {
        guard let device = AVCaptureDevice.default(for: .video) else { return }
        
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                
                if on == true {
                    device.torchMode = .on
                } else {
                    device.torchMode = .off
                }
                
                device.unlockForConfiguration()
            } catch {
                print("Torch could not be used")
            }
        } else {
            print("Torch is not available")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
        toggleTorch(on: false)
        UserDefaults.standard.synchronize()
    }
    
    @IBAction func photoButton(_ sender: Any) {
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.flashView.alpha = 1
        }, completion: nil)
        
        ProgressBar.isHidden = false
        AudioServicesPlayAlertSound(1108)
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.flashView.alpha = 0
        }, completion: nil)
        let snapShot = sceneView.snapshot()
        
        
        UIImageWriteToSavedPhotosAlbum(snapShot, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        
        if let error = error {
            print("Error Saving ARKit Scene \(error)")
        } else {
            photo.isEnabled = false
            UIView.animate(withDuration: 6.0, animations: {
                self.ProgressBar.value = 100
            }) { (Bool) in
                self.photo.isEnabled = true; AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                self.ProgressBar.isHidden = true
                self.ProgressBar.value = 0
            }
            if(!SalvaJpg){
                UserDefaults.standard.removeObject(forKey: "image1")
                let Jpg = UIImageJPEGRepresentation(image, 1.0)
                UserDefaults.standard.set(Jpg, forKey: "image1")
                print("saving image 1")
                SalvaJpg = true
            }else if(SalvaJpg){
                UserDefaults.standard.removeObject(forKey: "image2")
                let Jpg = UIImageJPEGRepresentation(image, 1.0)
                UserDefaults.standard.set(Jpg, forKey: "image2")
                SalvaJpg = false
                print("saving image 2")
            }
        }
    }
    @IBAction func resetButtonDidTouch(_ sender: UIBarButtonItem) {
        resetTrackingConfiguration()
    }
    
    func resetTrackingConfiguration() {
        guard let referenceImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil) else { return }
        let configuration = ARWorldTrackingConfiguration()
        configuration.detectionImages = referenceImages
        let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
        sceneView.session.run(configuration, options: options)
        sceneView.scene.rootNode.enumerateChildNodes { (node, stop) in
            node.removeFromParentNode()
        }
        label.text = "Move camera around to detect images"
    }
}

extension ViewController: ARSCNViewDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        DispatchQueue.main.async {
            
            guard let imageAnchor = anchor as? ARImageAnchor,
                let imageName = imageAnchor.referenceImage.name else { return }
            
            
            let overlayNode = self.getNode(withImageName: imageName)
            overlayNode.opacity = 0
            overlayNode.position.y = 0.2
            let planeGeometry = SCNPlane(width: 0.2, height: 0.35)
            let material = SCNMaterial()
            material.diffuse.contents = self.Image
            
            planeGeometry.materials = [material]
            
            
            let paintingNode = SCNNode(geometry: planeGeometry)
            
            paintingNode.transform = node.transform
            paintingNode.eulerAngles = node.eulerAngles
            paintingNode.position = node.position
            
            self.sceneView.scene.rootNode.addChildNode(paintingNode)
            node.addChildNode(overlayNode)
            //            self.sceneView.showsStatistics = true
            //            self.sceneView.debugOptions = ARSCNDebugOptions.showFeaturePoints
            
            
            
            self.label.text = "Image detected: \"\(imageName)\""
        }
    }
    
    func getPlaneNode(withReferenceImage image: ARReferenceImage) -> SCNNode {
        let plane = SCNPlane(width: image.physicalSize.width,
                             height: image.physicalSize.height)
        let node = SCNNode(geometry: plane)
        return node
    }
    
    func getNode(withImageName name: String) -> SCNNode {
        var node = SCNNode()
        switch name {
        case "img" :
            node = X
        default:
            break
        }
        return node
    }
    
}
