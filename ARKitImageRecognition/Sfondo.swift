

import UIKit

class Sfondo: UIViewController {
    @IBOutlet weak var imgv: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgv.alpha = 0
        overrideUserInterfaceStyle = .light

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 2.0, animations: {
            self.imgv.isHidden = false
            self.imgv.alpha = 1
        }) { (Bool) in
            self.performSegue(withIdentifier: "Vai", sender: self)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
