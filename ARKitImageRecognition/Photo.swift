
import Foundation
import UIKit
class Photo: UIViewController {
 
    var arrVie = [UIImageView]()
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
      img1.layer.cornerRadius = 8.0
      img1.clipsToBounds = true
        img2.layer.cornerRadius = 8.0
        img2.clipsToBounds = true
       img1.alpha = 0
       img2.alpha = 0
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(tapGestureRecognizer:)))
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(tapGestureRecognizer:)))
        
        
        self.img1.isUserInteractionEnabled = true
        self.img1.addGestureRecognizer(tapGestureRecognizer)
        self.img2.isUserInteractionEnabled = true
        self.img2.addGestureRecognizer(tapGestureRecognizer1)
        arrVie.append(img1)
        arrVie.append(img2)
        
    
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
           let popo = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Effetti") as! Effetti
           self.addChildViewController(popo)
           popo.view.frame = self.view.frame
            self.view.addSubview(popo.view)
           popo.didMove(toParentViewController: self)
           popo.img.image = arrVie[tapGestureRecognizer.view!.tag-1].image
          
           // Your action
       }
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.topItem?.title = "Last Photos"
        if let image = UserDefaults.standard.object(forKey: "image1") {
            img1.image = UIImage(data: image as! Data)
            img1.contentMode = .scaleAspectFill
        }
        if let image1 = UserDefaults.standard.object(forKey: "image2") {
            img2.image = UIImage(data: image1 as! Data)
            img2.contentMode = .scaleAspectFill
        }
        
        UIView.animate(withDuration: 2.0, animations: {
            self.img1.alpha = 1.0
            self.img2.alpha = 1.0
        })
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        img1.alpha = 0
        img2.alpha = 0
    }
}
